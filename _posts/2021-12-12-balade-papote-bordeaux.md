---
layout: post
title:  "12 décembre 2021 à Bordeaux (reportée)"
categories: rdv
city: Bordeaux
---
# Balade reportée

**Malheureusement, au vu de la situation sanitaire, et comme lors de la balade papote, on va faire de nombreux stop longs en lieu clos, nous préferons reporter à une date ultérieure.**



Qu'est-ce qui vous met en colère ou vous fait peur aujourd'hui ?

Je vais parler pour ma pomme.

En ce qui me concerne, ce qui me fait peur, ce sont les bouleversements dans notre quotidien qui vont bientôt avoir lieu en raison du changement climatique.

Ce qui me met en colère, c'est l'inaction des politiques au pouvoir sur ce sujet. C'est le fait que plutôt de tenter collectivement de répondre à cet enjeu inédit, on nous divertit en banalisant les idées d'extrême-droite et en fracturant un peu plus la société.

Ce qui me met en colère également, c'est qu'après 18 mois de pandémie, et 1 million de pauvres en plus, les 500 premières fortunes de France voient leur patrimoine augmenter de près de 300 milliards d'€

Vous partagez peut-être ces colères et ces craintes ? Vous avez peut-être d'autres colères, d'autres peurs qui vous animent ?

Si on prenait au sérieux nos colères et nos peurs en leur consacrant une journée entière pour en discuter et essayer ensemble de trouver quelques actions concrètes pour faire avancer le schmilblick ?

**Je vous propose une balade papote à {{ page.city }} le dimanche 12 décembre à 10h pétantes.**

J'imagine que si vous m'avez lu jusqu'ici, vous avez maintenant tout plein de questions qui trottent dans votre tête : concrètement, comment ça va se passer ? Ca côute combien ? Comment s'inscrire ? Vous trouverez [les réponses à vos questions par ici](/faq) !

Christophe Robillard
