---
layout: page
title: Foire aux questions
permalink: /faq-montpellier
organizers:
  - Audrey: audrey.bramy@scopyleft.fr
  - Christophe: christophe@robiweb.net
---

### Concrètement, comment ça se passe ?

L'idée, c'est d'échanger sur ce qui nous pose problème dans ce monde, et de trouver ensemble des actions concrètes.
Dans un bar, en binôme ou en groupe, on explore une partie du sujet, via des petits ateliers, présentations, questionnements ou autres expérimentations puis on passe à l'endroit suivant.
On profitera du trajet pour débriefer sur la session que l'on vient de faire.

### C'est quand et où ?

- La marche commencera le 12 décembre 2021 à 10h à Bordeaux (le lieu précis sera annoncé en temps et en heure)
- Et, ça se termine vers 19h pour enchainer sur des trucs encore plus informels.

### C'est pour qui ?

- Les personnes qui sont bavardes et qui ont des trucs à dire sur notre société
- Les personnes qui ne sont pas bavardes mais qui aiment écouter des personnes qui ont des trucs à dire sur notre société
- Les personnes qui ont envie de ne pas se limiter à discuter, qui veulent trouver des actions concrètes aux problèmes de notre époque

Bref, vous l'aurez compris, tout le monde peut venir !

### Je ne me sens pas au niveau…

Ne vous inquietez pas, nous non plus ;)

En vrai, on prendra soin de vous accompagner pour que vous puissiez explorer avec nous sans être largué.e. De toute façon, aucune connaissance n'est requise pour participer à une balade papote


### On sera combien ?

Au moins 2, maximum 10, probablement autour de 7.

### Il faut que je prévois des trucs ?

En gros, il faut que vous preniez votre bonne humeur et de quoi vous habiller confortablement pour marcher.

### On marche beaucoup ?

Une dizaine de kilomètres dans la journée.

On a calculé ça en se disant qu’on allait marcher 4 à 6 fois 20 minutes dans la journée.

Si ça vous semble beaucoup mais que vous avez envie de faire partie de l'aventure, parlez-nous en, on trouvera un truc.

### Je m'inscris où ?

Vous pouvez vous inscrire en envoyant un mail à [Audrey](mailto:audrey.bramy@scopyleft.fr) ou [Christophe](mailto:christophe@robiweb.net).

### Ça coûte combien cette petite escapade ?

On a estimé que ça coûterait 40€.

Pour ce montant, vous pourrez boire lorsque nous nous installerons dans des bars (à chaque session) et déjeuner.

Audrey et Christophe organisent ça bénévolement. S’il reste un peu d’argent à la fin d’une balade papote, il sera réutilisé pour une prochaine balade papote.

Si vous avez des questions sur le montant, on est très ouvert à en parler, n'hésitez pas à nous contacter.

### Et si c’est trop cher pour moi ?

Ne vous privez pas de cette super journée pour des problèmes matériels, contactez nous. Ensemble on va bien trouver une solution !

### On mangera quoi ?

On mangera au restaurant. Que vous soyez végétarien ou non, le restaurant proposera des plats adaptés à votre régime alimentaire.


### C'est à Montpellier, je n'habite pas Montpellier et l'hôtel, c'est cher et pas chaleureux. Je fais comment ?

Cela tombe bien, on a pensé à vous ! Il arrive parfois que des participant.e.s montpellierai·ne·s vous accueillent pour une nuit ou 2. Contactez [Audrey](mailto:audrey.bramy@scopyleft.fr), elle vous mettra en relation avec les participant·e·s qui peuvent vous accueillir.

### J'habite Montpellier et je peux héberger un·e participant·e la veille ou le soir de la balade papote. A qui donner cette info ?

Merci pour votre proposition ! Contactez [Audrey](mailto:audrey.bramy@scopyleft.fr).

### Et si j'ai une question ?

Vous pouvez contacter :
- [Audrey](mailto:audrey.bramy@scopyleft.fr).
- Christophe - [@twitter](http://twitter.com/krichtof) (dm ouvert) ou par [mail](mailto:{{site.email}})
