---
layout: page
show_in_menu: true
title: A propos
permalink: /a-propos/
---

{{ site.description }}

La [foire aux questions](/faq) répondra sûrement à toutes vos interrogations !

Ce format de journée est très fortement inspiré des [walkingdev](http://walkingdev.fr/)
